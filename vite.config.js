import {defineConfig} from "vite";
import {resolve} from "path";

export default  defineConfig({
  build: {
    lib: {
      entry: resolve(__dirname, 'src/query.ts'),
      name: 'ArrayQueryTs',
      fileName: 'query',
      formats: ["umd"]
    },
    sourcemap: true,
  }
})
